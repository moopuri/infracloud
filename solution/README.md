# The Soluction for PART I

1.Docker images have restart policy is "no", so that it will pull the image then it will create container and stopped immediately because of restart policy.
if you change the restart policy is "always" then it will start.

# Creating Random numbers

for this task we can achieve through  while loop condition.

vi gencsv.sh 
===============================

#!/bin/bash

n=8
i=2
while ((i<=n)); do
    echo "$((i++)),$RANDOM"
done > inputFile
=============================

# Created Dockerfile for pull image, copy, run, env variables, expose


vi Dockerfile
=================
FROM infracloudio/csvserver:latest
WORKDIR /infra
COPY gencsv.sh /infra
RUN ./gencsv.sh
ENV CSVSERVER_BORDER=Orange
EXPOSE 9393
ENTRYPOINT ["echo"]
CMD ["successfully container created"]

==========================================
